import os


class Config:
    def __init__(self):
        self.width = 1021
        self.height = 741
        self.tick = 0.02
        self.root = os.path.abspath(os.path.join(__file__, '..'))
        self.library = "PyQt5"
        self.doubleShoot = True
        self.simpleGame = False

config = Config()