class GameState:
    def __init__(self, angle=45, force=30, score=0, gravity=3):
        self._angle = angle
        self._force = force
        self._score = score
        self._gravity = gravity

    def acceptVisitor(self, visitor):
        visitor.setAngle(self._angle)
        visitor.setForce(self._force)
        visitor.setScore(self._score)
        visitor.setGravity(self._gravity)

    def getAngle(self):
        return self._angle

    def setAngle(self, angle):
        self._angle = angle

    def addAngle(self, angle):
        if self._angle > 90:
            self._angle = 90
        elif self._angle < -90:
            self._angle = -90
        self._angle += angle

    def getForce(self):
        return self._force

    def setForce(self, force):
        self._force = force

    def addForce(self, force):
        self._force += force

    def getScore(self):
        return self._score

    def setScore(self, score):
        self._score = score

    def addScore(self, score):
        self._score += score

    def getGravity(self):
        return self._gravity

    def setGravity(self, gravity):
        self._gravity = gravity

    def addGravity(self, gravity):
        self._gravity += gravity
