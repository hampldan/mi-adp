class AppBase:
    def __init__(self):
        raise NotImplementedError()

    def draw(self, entity):
        raise NotImplementedError()

    def update(self):
        raise NotImplementedError()

    def setForce(self, force):
        raise NotImplementedError()

    def setAngle(self, angle):
        raise NotImplementedError()

    def setGravity(self, gravity):
        raise NotImplementedError()

    def setScore(self, score):
        raise NotImplementedError()
