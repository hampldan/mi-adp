
from config import config


if config.library == "PyQt5":
    from graphics.qt5 import AppQt5 as App
else:
    raise Exception("Library not supported")

__all__ = [
    "App",
]
