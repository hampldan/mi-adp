import sys

from PyQt5 import uic
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QPaintEvent, QKeyEvent, QPixmap, QPainter
from PyQt5.QtWidgets import QMainWindow, QScrollArea, QWidget, QStackedLayout, QLabel
from PyQt5.QtWidgets import QApplication

from graphics.base import AppBase
from entities import MainEntity
from gameModel import GameModel
from controller import Controller
from config import config


qt_creator_file = f'{config.root}/ui/mainWindow.ui'
UiMainWindow, _ = uic.loadUiType(qt_creator_file)


class Canvas(QWidget):
    def __init__(self, gameModel, controller, app):
        super().__init__()
        self._controller = controller
        self._gameModel = gameModel
        self._app = app
        self.setFocusPolicy(Qt.ClickFocus)

    def paintEvent(self, event: QPaintEvent):
        self._gameModel.acceptVisitor(self._app)

    def keyPressEvent(self, event: QKeyEvent):
        if event.key() == Qt.Key_Up:
            self._controller.up()
        elif event.key() == Qt.Key_Down:
            self._controller.down()
        elif event.key() == Qt.Key_X:
            self._controller.x()
        elif event.key() == Qt.Key_Space:
            self._controller.space()
        elif event.key() == Qt.Key_Plus:
            self._controller.plus()
        elif event.key() == Qt.Key_Minus:
            self._controller.minus()
        elif event.key() == Qt.Key_G:
            self._controller.g()
        elif event.key() == Qt.Key_F:
            self._controller.f()
        elif event.key() == Qt.Key_Right:
            self._controller.right()
        elif event.key() == Qt.Key_Left:
            self._controller.left()
        elif event.key() == Qt.Key_C:
            self._controller.c()
        elif event.key() == Qt.Key_L:
            self._controller.l()
        elif event.key() == Qt.Key_S:
            self._controller.s()
        elif event.key() == Qt.Key_D:
            self._controller.d()

class Printer:
    def __init__(self, canvas):
        self._canvas = canvas
        self._pixmaps = {
            "bird": QPixmap(f'{config.root}/ui/assets/missile_transparent.png'),
            "cannon": QPixmap(f'{config.root}/ui/assets/cannon.png'),
            "piglet1": QPixmap(f'{config.root}/ui/assets/collision_transparent.png'),
            "piglet2": QPixmap(f'{config.root}/ui/assets/enemy1_transparent.png'),
            "piglet3": QPixmap(f'{config.root}/ui/assets/enemy2_transparent.png'),
        }

    def printEntity(self, entity: MainEntity):
        pixmap = self._pixmaps[entity.getPic()]
        painter = QPainter(self._canvas)
        painter.drawPixmap(entity.getX(), entity.getY(), pixmap.width(), pixmap.height(), pixmap)



class AppWindow(QMainWindow, UiMainWindow):
    def __init__(self, widget):
        QMainWindow.__init__(self)
        UiMainWindow.__init__(self)
        self.setupUi(self)

        canvas_holder = self.findChild(QScrollArea, 'canvasHolder')
        canvas_holder.setWidget(widget)
        canvas_holder.setStyleSheet('background-color: white')


class AppQt5(AppBase):
    def __init__(self, gameModel, controller):
        self._app = QApplication(sys.argv)
        gameModel.registerObserver(self)

        self._canvas = Canvas(gameModel, controller, self)
        self._printer = Printer(self._canvas)
        self._window = AppWindow(self._canvas)
        self._forceLabel = self._window.findChild(QLabel, 'forceLabel')
        self._angleLabel = self._window.findChild(QLabel, 'angleLabel')
        self._gravityLabel = self._window.findChild(QLabel, 'gravityLabel')
        self._scoreLabel = self._window.findChild(QLabel, 'scoreLabel')
        self._window.show()

        gameModel.start()
        ret = self._app.exec_()
        gameModel.exit()
        sys.exit(ret)

    def draw(self, entity):
        self._printer.printEntity(entity)

    def update(self):
        self._canvas.update()

    def setForce(self, force):
        self._forceLabel.setText(f"Force: {force}")

    def setAngle(self, angle):
        self._angleLabel.setText(f"Angle: {angle}")

    def setGravity(self, gravity):
        self._gravityLabel.setText(f"Gravity: {gravity}")

    def setScore(self, score):
        self._scoreLabel.setText(f"Score: {score}")
