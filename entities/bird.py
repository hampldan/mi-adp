from entities.mainEntity import MainEntity
from config import config


class BirdBase(MainEntity):
    def __init__(self, positionX=0, positionY=0):
        MainEntity.__init__(self, "bird", 30, 30, positionX, positionY)


class BirdInCannon(BirdBase):
    def __init__(self, positionX=0, positionY=0):
        BirdBase.__init__(self, positionX, positionY)


class Bird(BirdBase):
    def __init__(self, velocity, angle, gravity, strategy, positionX=0, positionY=0):
        BirdBase.__init__(self, positionX, positionY)
        self._velocity = velocity
        self._angle = angle
        self._gravity = gravity
        self._ticks = 0
        self._strategy = strategy

    def move(self, x, y):
        yAt = y + self._y
        xAt = x + self._x
        if yAt > config.height or xAt > config.width:
            return False
        self._x += x
        self._y += y
        return True

    def acceptVisitor(self, visitor):
        if self._y > config.height or self._x > config.width:
            return False
        visitor.draw(self)
        return True

    def fly(self):
        self._ticks+=1
        x, y = self._strategy(self._velocity, self._angle, self._gravity, self._ticks)
        return self.move(x, y)

