from PyQt5.QtGui import QPixmap
from config import config


class MainEntity(object):
    def __init__(self, imgPath, w, h, positionX=0, positionY=0):
        self._pic = imgPath
        self._w = w
        self._h = h
        self._x = positionX
        self._y = positionY

    def collision(self, entity):
        if (self._x <= entity.getX() + entity.getW()
            and self._x + self._w >= entity.getX()
            and self._y <= entity.getY() + entity.getH()
            and self._y + self._h >= entity.getY()):
            return True
        return False

    def move(self, x, y):
        yAt = y + self._y
        xAt = x + self._x
        if yAt < 0 or xAt < 0 or yAt + self._h > config.height or xAt + self._w > config.width:
            return False
        self._x += x
        self._y += y
        return True

    def acceptVisitor(self, visitor):
        visitor.draw(self)

    def clear(self):
        self._w = 0
        self._h = 0

    def getX(self):
        return self._x

    def setX(self, x):
        self._x = x

    def getY(self):
        return self._y

    def setY(self, y):
        self._y = y

    def getW(self):
        return self._w

    def setW(self, w):
        self._w = w

    def getH(self):
        return self._h

    def setH(self, h):
        self._h = h

    def getPic(self):
        return self._pic
