from entities.mainEntity import MainEntity
from random import seed, random
from config import config


class Piglet(MainEntity):
    def __init__(self, factory, imgPath, positionX=0, positionY=0):
        MainEntity.__init__(self, imgPath, 30, 30, positionX, positionY)
        self._factory = factory

    def getScore(self):
        return 1

    def die(self):
        pass

    def move(self):
        pass


class RunningPiglet(Piglet):
    def __init__(self, factory, imgPath, positionX=0, positionY=0):
        Piglet.__init__(self, factory, imgPath, positionX, positionY)
        self._vx = random() * 37 % 10
        self._vy = random() * 37 % 10

        a = int(random() * 61651) % 2
        b = int(random() * 61651) % 2

        if a == 0:
            self._vx *= -1
        if b == 0:
            self._vy *= -1

    def getScore(self):
        return 1

    def move(self):
        if self._x + self._vx <= 0 or self._x + self._vx + self._w  >= config.width:
            self._vx *= -1
        if self._y + self._vy <= 0 or self._y + self._vy + self._h  >= config.height:
            self._vy *= -1

        self._x += self._vx
        self._y += self._vy


class PigletT1(Piglet):
    def __init__(self, positionX=0, positionY=0):
        Piglet.__init__(self, factory, "piglet1", positionX, positionY)

    def getScore(self):
        return 1


class PigletT2(Piglet):
    def __init__(self, factory, positionX=0, positionY=0):
        Piglet.__init__(self, factory, "piglet2", positionX, positionY)

    def getScore(self):
        return 2

    def die(self):
        self._factory.createPigletT1(self._x, self._y)


class PigletT3(Piglet):
    def __init__(self, factory, positionX=0, positionY=0):
        Piglet.__init__(self, factory, "piglet3", positionX, positionY)

    def getScore(self):
        return 3

    def die(self):
        self._factory.createPigletT2(self._x, self._y)


class RunningPigletT1(RunningPiglet):
    def __init__(self, factory, positionX=0, positionY=0):
        RunningPiglet.__init__(self, factory, "piglet1", positionX, positionY)

    def getScore(self):
        return 1


class RunningPigletT2(RunningPiglet):
    def __init__(self, factory, positionX=0, positionY=0):
        RunningPiglet.__init__(self, factory, "piglet2", positionX, positionY)

    def getScore(self):
        return 2

    def die(self):
        self._factory.createPigletT1(self._x, self._y)


class RunningPigletT3(RunningPiglet):
    def __init__(self, factory, positionX=0, positionY=0):
        RunningPiglet.__init__(self, factory, "piglet3", positionX, positionY)

    def getScore(self):
        return 3

    def die(self):
        self._factory.createPigletT2(self._x, self._y)

