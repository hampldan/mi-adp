from entities.mainEntity import MainEntity
from entities.bird import BirdInCannon
from random import seed, random
from config import config
from threading import Timer
from time import sleep


class Cannon(MainEntity):
    def __init__(self, factory, positionX=0, positionY=0):
        MainEntity.__init__(self, "cannon", 30, 70, positionX, positionY)
        self._bird = BirdInCannon(positionX=positionX, positionY=positionY)
        self._doubleShot = config.doubleShoot
        self._factory = factory
        seed(68751)

    def move(self, x, y):
        yAt = self._y + y
        if yAt < 0 or yAt > config.height:
            return False
        self._bird.move(0,y)
        self._y += y
        return True

    def acceptVisitor(self, visitor):
        visitor.draw(self)
        self._bird.acceptVisitor(visitor)

    def _singleShot(self):
        self._factory.createBird(self._x, self._y)

    def shoooooooooot(self):
        self._singleShot()
        if self._doubleShot:
            Timer(0.25, self._singleShot).start()

    def changeState(self):
        self._doubleShot = not self._doubleShot
