from config import config
from services import TickTimer, SimpleFactory, SofisticatedFactory
from gameState import GameState
from copy import copy


class GameModel:
    def __init__(self):
        self._observers = set()
        self._state = GameState()
        self._birds = set()
        self._piglets = set()
        self._commands = set()
        self._cannon = None
        self._timer = TickTimer(config.tick)
        if config.simpleGame:
            self._factory = SimpleFactory(self._state, ModelProxy(self))
        else:
            self._factory = SofisticatedFactory(self._state, ModelProxy(self))
        self._factory.createCannon()
        self._save = None

    def registerObserver(self, observer):
        self._observers.add(observer)

    def unRegisterObserver(self, observer):
        self._observers.remove(observer)

    def start(self):
        self._timer.attach(self.tick)

    def acceptVisitor(self, visitor):
        self._state.acceptVisitor(visitor)
        self._cannon.acceptVisitor(visitor)
        for piglet in self._piglets.copy():
            piglet.acceptVisitor(visitor)
        for bird in self._birds.copy():
            bird.acceptVisitor(visitor)

    def moveCannon(self, offset_x: int, offset_y: int):
        self._cannon.move(offset_x, offset_y)
        for o in self._observers:
            o.update()

    def exit(self):
        self._timer.stop()

    def shoooooooooot(self):
        self._cannon.shoooooooooot()

    def spawnPiglet(self):
        self._factory.createPiglet()

    def tick(self):
        for command in self._commands.copy():
            command.execute()
            self._commands.remove(command)

        for bird in self._birds.copy():
            for piglet in self._piglets.copy():
                if bird.collision(piglet):
                    self.addScore(piglet.getScore())
                    self._piglets.remove(piglet)
            if not bird.fly():
                self._birds.remove(bird)
        for piglet in self._piglets.copy():
            piglet.move()
        for o in self._observers:
            o.update()

    def changeStrategy(self):
        self._factory.changeStrategy()

    def changeCannonState(self):
        self._cannon.changeState()

    def addPower(self, x):
        self._state.addForce(x)

    def addGravity(self, x):
        self._state.addGravity(x)

    def addScore(self, x=1):
        self._state.addScore(x)

    def addAngle(self, x):
        self._state.addAngle(x)

    def addBird(self, bird):
        self._birds.add(bird)

    def addPiglet(self, piglet):
        self._piglets.add(piglet)

    def addCannon(self, cannon):
        self._cannon = cannon

    def save(self):
        p = set()
        for a in self._piglets.copy():
            p.add(copy(a))
        s = copy(self._state)
        self._save = SaveProxy(SaveObject(p, s))

    def load(self):
        if self._save == None: return
        self._piglets = set()
        for a in self._save.getPiglets().copy():
            self._piglets.add(copy(a))
        self._state = copy(self._save.getState())
        self._factory.setState(self._state)

    def acceptCommand(self, command):
        self._commands.add(command)


class SaveObject:
    def __init__(self, piglets, state):
        self._piglets = piglets
        self._state = state

    def getPiglets(self):
        return self._piglets

    def getState(self):
        return self._state


class SaveProxy:
    def __init__(self, save):
        self._save = save

    def getPiglets(self):
        return self._save.getPiglets()

    def getState(self):
        return self._save.getState()



class ModelProxy:
    def __init__(self, model):
        self._model = model

    def addBird(self, bird):
        self._model.addBird(bird)

    def addPiglet(self, piglet):
        self._model.addPiglet(piglet)

    def addCannon(self, cannon):
        self._model.addCannon(cannon)
