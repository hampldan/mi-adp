class Command:
    def __init__(self, obj):
        self._obj = obj

    def execute(self):
        raise NotImplementedError

class ChangeCannonStateCommand(Command):
    def execute(self):
        self._obj.changeCannonState()

class SaveCommand(Command):
    def execute(self):
        self._obj.save()

class LoadCommand(Command):
    def execute(self):
        self._obj.load()

class SpawnPigletCommand(Command):
    def execute(self):
        self._obj.spawnPiglet()

class ChangeStrategyCommand(Command):
    def execute(self):
        self._obj.changeStrategy()

class AddGravityCommand(Command):
    def execute(self):
        self._obj.addGravity(1)

class RemoveGravityCommand(Command):
    def execute(self):
        self._obj.addGravity(-1)

class AddPowerCommand(Command):
    def execute(self):
        self._obj.addPower(1)

class RemovePowerCommand(Command):
    def execute(self):
        self._obj.addPower(-1)

class AddAngleCommand(Command):
    def execute(self):
        self._obj.addAngle(1)

class RemoveAngleCommand(Command):
    def execute(self):
        self._obj.addAngle(-1)

class MoveCannonUpCommand(Command):
    def execute(self):
        self._obj.moveCannon(0, -5)

class MoveCannonDownCommand(Command):
    def execute(self):
        self._obj.moveCannon(0, 5)

class ShootCommand(Command):
    def execute(self):
        self._obj.shoooooooooot()



class Controller:
    def __init__(self, gameModel):
        self._gameModel = gameModel

    def left(self):
        self._gameModel.acceptCommand(RemoveAngleCommand(self._gameModel))

    def right(self):
        self._gameModel.acceptCommand(AddAngleCommand(self._gameModel))

    def up(self):
        self._gameModel.acceptCommand(MoveCannonUpCommand(self._gameModel))

    def down(self):
        self._gameModel.acceptCommand(MoveCannonDownCommand(self._gameModel))

    def space(self):
        self._gameModel.acceptCommand(ShootCommand(self._gameModel))

    def plus(self):
        self._gameModel.acceptCommand(AddPowerCommand(self._gameModel))

    def minus(self):
        self._gameModel.acceptCommand(RemovePowerCommand(self._gameModel))

    def f(self):
        self._gameModel.acceptCommand(RemoveGravityCommand(self._gameModel))

    def g(self):
        self._gameModel.acceptCommand(AddGravityCommand(self._gameModel))

    def x(self):
        self._gameModel.acceptCommand(ChangeStrategyCommand(self._gameModel))

    def c(self):
        self._gameModel.acceptCommand(SpawnPigletCommand(self._gameModel))

    def s(self):
        self._gameModel.acceptCommand(SaveCommand(self._gameModel))

    def l(self):
        self._gameModel.acceptCommand(LoadCommand(self._gameModel))

    def d(self):
        self._gameModel.acceptCommand(ChangeCannonStateCommand(self._gameModel))
