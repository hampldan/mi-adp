from entities.mainEntity import MainEntity


def testCollisionSameSize():
    a = MainEntity("", 10, 10, 0, 0)
    b = MainEntity("", 10, 10, 11, 11)
    c = MainEntity("", 10, 10, 8, 8)
    d = MainEntity("", 10, 10, 0, 11)

    assert not a.collision(b)
    assert not b.collision(a)

    assert a.collision(c)
    assert c.collision(a)
    assert b.collision(c)
    assert c.collision(b)

    assert not a.collision(d)
    assert not d.collision(a)
    assert not b.collision(d)
    assert not d.collision(b)

    assert c.collision(d)
    assert d.collision(c)

def testCollisionBigAndSmall():
    a = MainEntity("", 100, 100, 0, 0)
    b = MainEntity("", 10, 10, 10, 10)
    c = MainEntity("", 10, 10, 110, 110)

    assert a.collision(b)
    assert b.collision(a)

    assert not a.collision(c)
    assert not c.collision(a)
    assert not b.collision(c)
    assert not c.collision(b)
