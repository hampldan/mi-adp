from services.factory import Factory, SimpleFactory, SofisticatedFactory
from mock import patch, MagicMock
from config import config


@patch("services.factory.Bird")
@patch("services.factory.PigletT1")
@patch("services.factory.PigletT2")
@patch("services.factory.PigletT3")
@patch("services.factory.RunningPigletT1")
@patch("services.factory.RunningPigletT2")
@patch("services.factory.RunningPigletT3")
@patch("services.factory.Cannon")
@patch("services.factory.oblique")
@patch("services.factory.ballistic")
def testFactory(mBallistic, mOblique, mCannon, mRPT3, mRPT2, mRPT1, mPT3, mPT2, mPT1, mBird):
    model = MagicMock()
    state = MagicMock()
    state.getForce.return_value = 0
    state.getAngle.return_value = 0
    state.getGravity.return_value = 0
    state.getForce.return_value = 0

    f = Factory(state, model)
    for i in range(100):
        x,y = f.getRandXY()
        assert x >= 0
        assert x <= config.width
        assert y >= 0
        assert y <= config.height

    f.createBird(0, 0)
    assert model.addBird.call_count == 1

    f.createCannon()
    assert model.addCannon.call_count == 1

    try:
        f.createPiglet()
        assert False
    except NotImplementedError:
        pass
    assert model.addPiglet.call_count == 0

@patch("services.factory.Bird")
@patch("services.factory.PigletT1")
@patch("services.factory.PigletT2")
@patch("services.factory.PigletT3")
@patch("services.factory.RunningPigletT1")
@patch("services.factory.RunningPigletT2")
@patch("services.factory.RunningPigletT3")
@patch("services.factory.Cannon")
@patch("services.factory.oblique")
@patch("services.factory.ballistic")
def testSimpleFactory(mBallistic, mOblique, mCannon, mRPT3, mRPT2, mRPT1, mPT3, mPT2, mPT1, mBird):
    model = MagicMock()
    state = MagicMock()
    state.getForce.return_value = 0
    state.getAngle.return_value = 0
    state.getGravity.return_value = 0
    state.getForce.return_value = 0

    f = SimpleFactory(state, model)
    for i in range(100):
        x,y = f.getRandXY()
        assert x >= 0
        assert x <= config.width
        assert y >= 0
        assert y <= config.height

    f.createBird(0, 0)
    assert model.addBird.call_count == 1

    f.createCannon()
    assert model.addCannon.call_count == 1

    f.createPiglet()
    assert model.addPiglet.call_count == 1


@patch("services.factory.Bird")
@patch("services.factory.PigletT1")
@patch("services.factory.PigletT2")
@patch("services.factory.PigletT3")
@patch("services.factory.RunningPigletT1")
@patch("services.factory.RunningPigletT2")
@patch("services.factory.RunningPigletT3")
@patch("services.factory.Cannon")
@patch("services.factory.oblique")
@patch("services.factory.ballistic")
def testSofisticatedFactory(mBallistic, mOblique, mCannon, mRPT3, mRPT2, mRPT1, mPT3, mPT2, mPT1, mBird):
    model = MagicMock()
    state = MagicMock()
    state.getForce.return_value = 0
    state.getAngle.return_value = 0
    state.getGravity.return_value = 0
    state.getForce.return_value = 0

    f = SofisticatedFactory(state, model)
    for i in range(100):
        x,y = f.getRandXY()
        assert x >= 0
        assert x <= config.width
        assert y >= 0
        assert y <= config.height

    f.createBird(0, 0)
    assert model.addBird.call_count == 1

    f.createCannon()
    assert model.addCannon.call_count == 1

    f.createPiglet()
    assert model.addPiglet.call_count == 1


