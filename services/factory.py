from entities import Bird, PigletT1, PigletT2, PigletT3, RunningPigletT1, RunningPigletT2, RunningPigletT3, Cannon
from services.strategies import oblique, ballistic
from random import seed, random
from config import config


class Factory:
    def __init__(self, state, model):
        self._state = state
        self._model = model
        self._strategy = oblique
        seed(16415315)

    def changeStrategy(self):
        if self._strategy == oblique:
            self._strategy = ballistic
        else:
            self._strategy = oblique

    def createBird(self, x, y):
        self._model.addBird(Bird(self._state.getForce(), self._state.getAngle(), self._state.getGravity(), self._strategy, x, y))

    def createCannon(self):
        self._model.addCannon(Cannon(self, 0, config.height/3))

    def createPiglet(self):
        raise NotImplementedError()

    def setState(self, state):
        self._state = state

    def getRandXY(self):
        x = (abs(random() * 31415) % (config.width - 100)) + 50
        y = abs(random() * 31415) % config.height
        return x, y


class SimpleFactory(Factory):
    def createPiglet(self):
        x, y = self.getRandXY()
        t = int(random() * 6135) % 3
        if t == 2:
            self.createPigletT3(x, y)
        elif t == 1:
            self.createPigletT2(x, y)
        else:
            self.createPigletT1(x, y)

    def createPigletT1(self, x, y):
        self._model.addPiglet(PigletT1(self, x, y))

    def createPigletT2(self, x, y):
        self._model.addPiglet(PigletT2(self, x, y))

    def createPigletT3(self, x, y):
        self._model.addPiglet(PigletT3(self, x, y))


class SofisticatedFactory(Factory):
    def createPiglet(self):
        x, y = self.getRandXY()
        t = int(random() * 6135) % 3
        if t == 2:
            self.createPigletT3(x, y)
        elif t == 1:
            self.createPigletT2(x, y)
        else:
            self.createPigletT1(x, y)


    def createPigletT1(self, x, y):
        self._model.addPiglet(RunningPigletT1(self, x, y))

    def createPigletT2(self, x, y):
        self._model.addPiglet(RunningPigletT2(self, x, y))

    def createPigletT3(self, x, y):
        self._model.addPiglet(RunningPigletT3(self, x, y))

