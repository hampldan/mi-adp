from threading import Timer


class TickTimer(object):
    def __init__(self, tick=0.05):
        self._registered = set()
        self._tick = tick
        self._timer = None
        self._stop = False
        self._startTimer()

    def attach(self, callback):
        self._registered.add(callback)

    def detach(self, callback):
        self._registered.remove(callback)

    def _startTimer(self):
        self._timer = Timer(self._tick, self._notifyAll)
        if self._stop: return
        self._timer.start()

    def _notifyAll(self):
        for callback in self._registered.copy():
            callback()
        self._startTimer()

    def stop(self):
        self._registered.clear()
        self._stop = True
        self._timer.cancel()