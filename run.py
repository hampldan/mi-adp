from gameModel import GameModel
from controller import Controller
from graphics import App


if __name__ == '__main__':
    gameModel = GameModel()
    controller = Controller(gameModel)
    app = App(gameModel, controller)
