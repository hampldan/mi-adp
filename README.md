# MI-ADP Python template

This template is a *starting point* for all of those who wants to implement their 
semestral work from the MI-ADP course in Python programming language. 

It is deliberately dumb and incorrectly structured - it is your goal to make it 
beautiful and follow all the best coding practices and design/architecural 
patterns you'll learn throughout the semester.

## How to run

1. Make sure you have Python3 installed: `python3 --version`
2. Clone the repository: `git clone git@gitlab.fit.cvut.cz:drahoja9/mi-adp-python-template.git your_project_name`
3. Go to the root of the app: `cd your_project_name`
4. Create virtual environment: `python3 -m venv your_venv_dir`
5. Activate it: `source your_venv_dir/bin/activate`
6. Install all requirements: `pip install -r requirements.txt`
7. Run the app: `python3 run.py`


## Task
### MVC game
elaboration: individually 
submission: 
personally at the last tutorial 
mandatory VCS = school gitlab 
### patterns:
1) [MVC](https://miadp.fit.cvut.cz/library/detail/38)
1) [Strategy](https://miadp.fit.cvut.cz/library/detail/53)
1) [Bridge](https://miadp.fit.cvut.cz/library/detail/64)
1) [Proxy](https://miadp.fit.cvut.cz/library/detail/56)
1) [State](https://miadp.fit.cvut.cz/library/detail/53)
1) [Visitor](https://miadp.fit.cvut.cz/library/detail/55)
1) [Observer](https://miadp.fit.cvut.cz/library/detail/49)
1) [Command](https://miadp.fit.cvut.cz/library/detail/57)
1) [Memento](https://miadp.fit.cvut.cz/library/detail/58)
1) [Abstract factory](https://miadp.fit.cvut.cz/library/detail/51)
### required properties:
force change 
angle of the cannon (angle) 
gravity 
score counting 
reduction of dependency on a specific graphics library (Bridge) 
possibility to fire multiple shots at once (State) 
model control by commands (Command) 
step-back (Memento & Command) 
2 missile movement strategies (Strategy) 
at least 5 unit cases (test cases) in at least 2 test suites and Mocking 


## Design
1) MVC - separation to model view and controller
2) Strategy - Bird flies in an oblique or ballistic curve
3) Bridge - Separation of UI
4) Proxy - access through separate entity for example saveProxy
5) State - Cannon shoots one or multiple birds
6) Visitor - Game render visits each element and renders it
7) Observer - Notifies render after changes
8) Command - Controller creates command and sends it to model, which then resolves it
9) Memento - game snapshot - no one can change it apart from game model
10) Abstract factory - moving/stationary piglets



## Controlls

 - Left Right - angle
 - f g - gravity
 - Up Down - move cannon
 - x - change mode
 - c - spawn a piglet
 - d - change cannon state
 - s l - save & load